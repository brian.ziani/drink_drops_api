const express       = require('express');
const bodyParser    = require('body-parser');
const mongoose      = require('mongoose');
const graphqlHTTP   = require('express-graphql');
const {buildSchema} = require('graphql');
const path          = require('path');

const app = express();

app.use(bodyParser.json());

app.get('/', (req, res, next) => {
    res.send("hello world !")
});

mongoose.connect('mongodb://localhost:27017/drink_drops_local', {useNewUrlParser: true, useUnifiedTopology:true}).then(() => {
    app.listen(3000);
}).catch(err => {
    console.log(err);
});

